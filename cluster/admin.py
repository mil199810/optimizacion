from django.contrib import admin
from import_export import resources
from import_export.admin import ImportExportModelAdmin
from .models import InstanciaRuteo, Cliente, Tiendas, Departamento, Municipio


class ClientesAdmin(ImportExportModelAdmin):
    pass


class TiendasAdmin(ImportExportModelAdmin):
    pass


class DepartamentoAdmin(ImportExportModelAdmin):
    pass


class MunicipioAdmin(ImportExportModelAdmin):
    pass

# Register your models here.
admin.site.register(InstanciaRuteo)
admin.site.register(Cliente, ClientesAdmin)
admin.site.register(Tiendas, TiendasAdmin)
admin.site.register(Departamento, DepartamentoAdmin)
admin.site.register(Municipio, MunicipioAdmin)


