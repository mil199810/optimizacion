from django.contrib.auth.models import User
from django.db import models
from django.urls import reverse

# Create your models here.

class InstanciaRuteo(models.Model):
    id = models.AutoField(primary_key = True)
    descripcion = models.TextField('descripcion', max_length=100)
    user = models.ForeignKey(User, on_delete = models.CASCADE)

    def __str__(self):
        return str(self.id)


class Cliente(models.Model):
    id = models.AutoField(primary_key = True)
    coor_x = models.FloatField(name='coor_x')
    coor_y = models.FloatField(name='coor_y')
    instancia = models.ForeignKey(InstanciaRuteo, on_delete = models.CASCADE, blank=True, null=True)

    def __str__(self):
        return str(self.id)


class Tiendas(models.Model):
    id = models.AutoField(primary_key=True)
    instancia = models.ForeignKey(InstanciaRuteo, on_delete = models.CASCADE)
    coor_x = models.FloatField(name='coor_x')
    coor_y = models.FloatField(name='coor_y')

    def __str__(self):
        return str(self.id)


class Departamento(models.Model):
    id = models.IntegerField('id')
    codigo_departamento = models.IntegerField(primary_key=True)
    nombre_departamento = models.TextField('nombre_departamento')

    def __str__(self):
        return str(self.nombre_departamento) 


class Municipio(models.Model):
    id = models.IntegerField('id')
    codigo_departamento = models.ForeignKey(Departamento, on_delete = models.CASCADE, blank=True, null=True)
    codigo_dpto_mpio = models.IntegerField(primary_key=True)
    nombre_municipio = models.TextField('nombre_municipio')
    lon = models.FloatField('longitud')
    lat = models.FloatField('latitud')

    def __str__(self):
        return str(self.nombre_municipio)    
