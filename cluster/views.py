#from pyomo.environ import * 
#from pyomo.opt import SolverFactory
from django.views.generic import CreateView
from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from .forms import InstanciaForm
from .models import InstanciaRuteo, Cliente, Tiendas
from .utils import optimizar



def home(request):
    #instance.pprint()
    #instance.Y.display()
    optimizar()
    print('atras bueno')
    contexto={}
    return render(request, 'cluster/index.html', contexto)


class InstanciaCreateView(CreateView):
    model = InstanciaRuteo
    form_class = InstanciaForm
    template_name = 'cluster/instancia.html'
    

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        context = {}
        if form.is_valid():
            instancia = form.save(commit=False)
            instancia.user = request.user
            instancia.save()
            return redirect('cluster:agregar_instancia')
        else:
            form = self.form_class()
            context['form'] = form
            return render(request,self.template_name, context=context)
        
