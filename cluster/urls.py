from django.conf.urls import url
from django.urls import path, include
from .views import InstanciaCreateView
from . import views

app_name = 'cluster'
urlpatterns = [
    path('', views.home, name='home'),    
    path('instancia/agregar', InstanciaCreateView.as_view(), name='agregar_instancia')
]
