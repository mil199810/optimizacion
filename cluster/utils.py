from .models import InstanciaRuteo, Cliente, Tiendas
from pyomo.environ import *
from pyomo.opt import SolverFactory
import math
import logging




def optimizar():
   #from pyomo.opt import SolverFactory
    model = AbstractModel() #Se crea el modelo
    logging.getLogger('pyomo.core').setLevel(logging.ERROR)
    #model.CLI= RangeSet(1,5,1)
    model.TNDS = RangeSet(1,5,1) ##Representan los puntos de atención los cuales satisfacen la demanda generada por los clientes, las tiendas se encuentran representados por el conjunto TNDS e indexadas en j.
    #Parametros
    #model.cordclix =  Param(model.CLI)
    #coorxclie = []

    clientes = Cliente.objects.filter(instancia = 1)
    categoriapregrado = []
    for Opt1 in clientes:
          categoriapregrado.append(Opt1.id)
    model.CLI = Set(initialize = categoriapregrado) 
    
    clientes = Cliente.objects.filter(instancia = 1)
    tiendas = Tiendas.objects.filter(instancia = 1)
    # print()
    # for i in clientes:
    #     coorxclie.append(i.coor_x)
    # print(coorxclie)

    def coor_x_c(model, i):
        clien = Cliente.objects.get(instancia = 1,pk=i)
        return(clien.coor_x)
    model.cordclix = Param(model.CLI, initialize = coor_x_c)

    print(type(model.cordclix))
    def coor_y_c(model, i):
        return(clientes[i-1].coor_y)
    model.cordcliy = Param(model.CLI, initialize = coor_y_c)

    def coor_x_t(model, i):
        return(tiendas[i-1].coor_x)
    model.cordtndx = Param(model.TNDS, initialize = coor_x_t)

    def coor_y_t(model, i):
        return(tiendas[i-1].coor_y)
    model.cordtndy = Param(model.TNDS, initialize = coor_y_t)
    
    #model.cordcliy =  Param(model.CLI)
    #Coordenadas  del cliente i
    #model.cordtndx =  Param(model.TNDS)
    #model.cordtndy =  Param(model.TNDS)
    #Coordenadas  de la tienda j
    def dstcltn_init(model,i,j ):
        return math.sqrt( ( (model.cordclix[i]-model.cordtndx[j])**2 +(model.cordcliy[i]-model.cordtndy[j])**2 ) )
    model.dstcltn = Param(model.CLI , model.TNDS,initialize=dstcltn_init)
    #distancia entre clientes y tiendas 
    def destmax_init(model):
        return 0.2
    model.destmax =  Param(initialize=destmax_init)
    #desviación máxima permitida en porcentaje de clientes 
    #asignados a una tienda con respecto al promedio.
    def nclipro_init(model):
        return len(model.CLI)/len(model.TNDS)
    model.nclipro =  Param(initialize=nclipro_init)
    #Numero de clientes promedios
    #Variables
    model.Y = Var(model.CLI, model.TNDS,within=Binary)
    #Variable binaria que toma el valor 1 si el cliente i
    #es asignado a la tienda j
    #Funcion objetivo
    def compactacion(model):
        return sum(model.dstcltn[i,j]*model.Y[i,j] for i in model.CLI for j in model.TNDS)
    model.Comp = Objective(rule=compactacion, sense=minimize)
    #Restricciones
    #R1   
    def R1_init(model,i):
        return sum(model.Y[i,j] for j in model.TNDS) == 1
    model.R1 =  Constraint(model.CLI, rule=R1_init)
    def R2_init(model,j):
        return sum(model.Y[i,j] for i in model.CLI) <= (1+model.destmax)*model.nclipro
    model.R2 = Constraint(model.TNDS, rule=R2_init)
    def R3_init(model,j):
        return sum(model.Y[i,j] for i in model.CLI) >= (1-model.destmax)*model.nclipro
    model.R3 = Constraint(model.TNDS, rule=R3_init)
    
    #Datos
    # data = DataPortal()
    # print(type(data))
    # data.load(filename='cordclix.tab',param=model.cordclix,format=)
    # data.load(filename='cordcliy.tab',param=model.cordcliy)
    # data.load(filename='cordtndx.tab',param=model.cordtndx)
    # data.load(filename='cordtndy.tab',param=model.cordtndy)
    opt = SolverFactory('cplex')
    #opt = SolverFactory('glpk')
    instance = model.create_instance()
    print(instance.cordclix._data)
    print(instance.cordcliy._data)
    print(instance.dstcltn._data)
    results = opt.solve(instance)
    instance.Y.display()
    print('Esto corrio') 
    instance.pprint()