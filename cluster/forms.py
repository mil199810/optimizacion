from django import forms
from .models import InstanciaRuteo


class InstanciaForm(forms.ModelForm):
    class Meta:
        model = InstanciaRuteo
        fields = (
            'descripcion', 
        )
        label = {
            'descripcion' : 'Descripción de la intancia'
        }
        widgets = {
            'descripcion' : forms.Textarea(
                attrs = {
                    'class': 'form-control',
                    'placeholder' : 'Ingrese el nombre de intancia',
                }
            )
        }


