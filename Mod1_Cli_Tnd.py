# -*- coding: utf-8 -*-
"""
Created on Thu May 21 21:04:27 2020

@author: JUAN
"""


from pyomo.environ import * # Se importa la librería de pyomo
from pyomo.opt import SolverFactory
model = AbstractModel() #Se crea el modelo

import math  

import logging

logging.getLogger('pyomo.core').setLevel(logging.ERROR)

#Se inicializa los conjuntos
#model.CLI = Set()
#model.TNDS = Set()
model.CLI = RangeSet(1,200,1) #Representan al grupo de clientes que generan la demanda de recorridos, los clientes se encuentran representados por el conjunto CLI e indexadas en i.
model.TNDS = RangeSet(1,10,1) ##Representan los puntos de atención los cuales satisfacen la demanda generada por los clientes, las tiendas se encuentran representados por el conjunto TNDS e indexadas en j.

#Parametros
model.cordclix =  Param(model.CLI)
model.cordcliy =  Param(model.CLI)
#Coordenadas  del cliente i

model.cordtndx =  Param(model.TNDS)
model.cordtndy =  Param(model.TNDS)
#Coordenadas  de la tienda j


def dstcltn_init(model,i,j ):
    return math.sqrt( ( (model.cordclix[i]-model.cordtndx[j])**2 +(model.cordcliy[i]-model.cordtndy[j])**2 ) )
model.dstcltn = Param(model.CLI , model.TNDS,initialize=dstcltn_init)
#distancia entre clientes y tiendas 

def destmax_init(model):
    return 0.2
model.destmax =  Param(initialize=destmax_init)
#desviación máxima permitida en porcentaje de clientes 
#asignados a una tienda con respecto al promedio.

def nclipro_init(model):
    return len(model.CLI)/len(model.TNDS)
model.nclipro =  Param(initialize=nclipro_init)
#Numero de clientes promedios

#Variables
model.Y = Var(model.CLI, model.TNDS,within=Binary)
#Variable binaria que toma el valor 1 si el cliente i
#es asignado a la tienda j

#Funcion objetivo
def compactacion(model):
    return sum(model.dstcltn[i,j]*model.Y[i,j] for i in model.CLI for j in model.TNDS)
model.Comp = Objective(rule=compactacion, sense=minimize)

#Restricciones
#R1   
def R1_init(model,i):
    return sum(model.Y[i,j] for j in model.TNDS) == 1
model.R1 =  Constraint(model.CLI, rule=R1_init)

def R2_init(model,j):
    return sum(model.Y[i,j] for i in model.CLI) <= (1+model.destmax)*model.nclipro
model.R2 = Constraint(model.TNDS, rule=R2_init)

def R3_init(model,j):
    return sum(model.Y[i,j] for i in model.CLI) >= (1-model.destmax)*model.nclipro
model.R3 = Constraint(model.TNDS, rule=R3_init)

#Datos
data = DataPortal()
data.load(filename='Mod1_V2.xlsx', range='Clicordclix', 
                    param=model.cordclix, index=model.CLI)

data.load(filename='Mod1_V2.xlsx', range='Clicordcliy', 
                    param=model.cordcliy, index=model.CLI)

data.load(filename='Mod1_V2.xlsx', range='Tndcordtndx', 
                    param=model.cordtndx, index=model.CLI)

data.load(filename='Mod1_V2.xlsx', range='Tndcordtndy', 
                    param=model.cordtndy, index=model.CLI)
print(model.dstcltn)
opt = SolverFactory('cplex')
#opt = SolverFactory('glpk')

instance = model.create_instance(data)

print(instance.CLI._len)
results = opt.solve(instance) # solves and updates instance
#instance.pprint()
#instance.Y.display()

