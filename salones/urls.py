from django.conf.urls import url
from django.urls import path, include
from . import views
from django.conf.urls.static import static

app_name = 'salones'
urlpatterns = [
    path('', views.home, name='home'),  
    path('sheduling/<str:pk_salon>/', views.scheduling, name='sheduling'),
    path('sheduling/<str:pk_pro>/<int:pk_sem>/', views.scheduling_pro_sem, name='sheduling_pro_sem')  
    #path('instancia/agregar', InstanciaCreateView.as_view(), name='agregar_instancia')
]
