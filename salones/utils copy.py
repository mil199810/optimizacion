from pyomo.environ import * # Se importa la librería de pyomo
from pyomo.opt import SolverFactory
import math
import logging
from .models import AsignaturaPregrado, Dia, Salon, FranjaHoraria, ProgramaPregrado, ProgramaPosgrado, Profesor, CategoriaPregrado, CategoriaPosgrado, W, DiaPregrado, DiaPosgrado, SalonPregrado, SalonPosgrado, FranjaPregrado, FranjaPosgrado, AsignaturaGrupoMovilidad, PregradoSemestreAsignatura, PosgradoSemestreSesion

def read_data():
    asignaturas = AsignaturaPregrado.objects.filter(instancia = 1)
    


def optimizar(ins):
     model = AbstractModel() #Se crea el modelo
     logging.getLogger('pyomo.core').setLevel(logging.ERROR)
     #Se inicializa los conjuntos

     #Asinaturas pregrado
     asignaturas = AsignaturaPregrado.objects.filter(instancia = ins)
     asignatura=[]
     kpregrado = []
     for i in asignaturas:
          asignatura.append(i.abreviatura)
          kpregrado.append(i.categoriapregrado.abreviatura)
     model.I = Set(initialize = asignatura)

     #Dias programabables
     dias = Dia.objects.filter(instancia = ins)
     dia = []
     for d in dias:
          dia.append(d.abreviatura)
     model.D = Set(initialize = dia)

     #Salones programables
     salones = Salon.objects.filter(instancia = ins)
     salon = []
     capacidad = []
     movilidadreducida = []
     for r in salones:
          salon.append(r.abreviatura)
          capacidad.append(r.capacidad)
          movilidadreducida.append(r.movilidad_reducida)
     model.R = Set(initialize = salon)  

     #Franjas horarias programables
     franjas = FranjaHoraria.objects.filter(instancia = ins)
     franja = []
     for f in franjas:
          franja.append(f.abreviatura)
     model.F = Set(initialize = franja) 

     #Programas pregrado
     programapregrados = ProgramaPregrado.objects.filter(instancia = ins)
     programapregrado = []
     for p1 in programapregrados:
          programapregrado.append(p1.abreviatura)
     model.P1 = Set(initialize = programapregrado) 

     #Programas posgrado
     programaposgrados = ProgramaPosgrado.objects.filter(instancia = ins)
     programapos = []
     kposgrado = []
     semestrespos = []
     for p2 in programaposgrados:
          programapos.append(p2.abreviatura)
          kposgrado.append(p2.categoriaposgrado.abreviatura)
          semestrespos.append(p2.semestres)

     model.P2 = Set(initialize = programapos) 

     #Profesores
     profesores = Profesor.objects.filter(instancia = ins)
     profesor = []
     for teacher in profesores:
          profesor.append(teacher.nombre)
     model.teacher = Set(initialize = profesor) 

     model.direct = Set(within=model.teacher) # Profesores directivas
     model.AsigTeach = Set(model.teacher, within=model.I) # asignaturas de profesores

     Ws = W.objects.filter(instancia = ins)
     dias_w = []
     for dw in Ws:
          dias_w.append(dw.dia1.abreviatura)
     print(dias_w)
     model.W = Set(model.D, within=model.D, initialize = dias_w)

     def cap_sal(model, i):
        salon=Salon.objects.get(pk=i)
        return(salon.capacidad)
     model.C = Param(model.R, initialize = cap_sal) # Capacidad en numero de estudiantes del espacio fisico r.

     #model.C= Param(model.R, initialize = capacidad) 
     
     def movil_reduc(model, i):
        salon=Salon.objects.get(pk=i)
        return(salon.movilidad_reducida)
     model.Z = Param(model.R, initialize = movil_reduc) # 1, si el espacio fisico r es apto para estudiantes de movilidad reducida.
     
     #model.Z= Param(model.R, initialize = movilidadreducida) 
     model.ZOOM= Param(within=PositiveIntegers) # Pantallas simultameas maximas en zoom


     # categorias de pregrados
     categoriapregrados = CategoriaPregrado.objects.filter(instancia = ins)
     categoriapregrado = []
     for Opt1 in categoriapregrados:
          categoriapregrado.append(Opt1.abreviatura)
     model.Opt1 = Set(initialize = categoriapregrado) 

     # categorias de posgrado
     categoriaposgrados = CategoriaPosgrado.objects.filter(instancia = ins)
     categoriaposgrado = []
     for Opt2 in categoriaposgrados:
          categoriaposgrado.append(Opt2.abreviatura)
     model.Opt2 = Set(initialize = categoriaposgrado) 

     model.K2 = Set(model.P2, within=model.Opt2, initialize =  kposgrado) # categoria al que pertenece el posgrado p2

     # Dias programables para la categoria o1
     diapregrados = DiaPregrado.objects.filter(instancia = ins)
     diapregrado = []
     for D1 in diapregrados:
          diapregrado.append(D1.dia.abreviatura)
     #model.D1 = Set(model.Opt1, within=model.D, initialize = diapregrado) # Dias programables para la categoria o1
     
     def dia_pre(model):
          for opt1 in model.Opt1 :
               for d in model.D:
                    asignatura=DiaPregrado.objects.get(categoriapregrado=opt1,dia=d)
                    return(asignatura.dia.abreviatura)
     model.D1 = Set(model.Opt1, within=model.D, initialize = diapregrado) # Dias programables para la categoria o1

     # Dias programables para la categoria o2
     diaposgrados = DiaPosgrado.objects.filter(instancia = ins)
     diaposgrado = []
     for D2 in diaposgrados:
          diaposgrado.append(D2.dia.abreviatura)  
     model.D2 = Set(model.Opt2, within=model.D, initialize = diaposgrado) # Dias programables para la categoria o2


     # Salones de clase disponibles para la categoria o1
     salonpregrados = SalonPregrado.objects.filter(instancia = ins)
     salonpregrado = []
     for R1 in salonpregrados:
          salonpregrado.append(R1.salon.abreviatura)
     model.R1 = Set(model.Opt1, within=model.R, initialize = salonpregrado) # Salones de clase disponibles para la categoria o1
     
     # Salones de clase disponibles para la categoria o2
     salonposgrados = SalonPosgrado.objects.filter(instancia = ins)
     salonposgrado = []
     for R2 in salonposgrados:
          salonposgrado.append(R2.salon.abreviatura)
     model.R2 = Set(model.Opt2, within=model.R, initialize = salonposgrado) # Salones de clase disponibles para la categoria o2

     def DO1_init(model):
          return ((o1,d1) for o1 in model.Opt1 for d1 in model.D1[o1])
     model.DO1=Set(dimen=2, initialize=DO1_init)

     def DO2_init(model):
          return ((o2,d2) for o2 in model.Opt2 for d2 in model.D2[o2])
     model.DO2=Set(dimen=2, initialize=DO2_init)

     # Franjas disponibles para la categoria o1
     def fran_pre(model):
          for o1,d1 in model.DO1 :
               for f in model.F:
                    asignatura=FranjaPregrado.objects.get(categoriapregrado=o1,dia=d1,franjahoraria=f)
                    return(asignatura.franjahoraria.abreviatura)
     model.F1 = Param(model.DO1, within=model.F, initialize = fran_pre)

     # Franjas disponibles para la categoria o2
     def fran_pos(model):
          for o2,d2 in model.DO2 :
               for f in model.F:
                    asignatura=FranjaPosgrado.objects.get(categoriaposgrado=o2,dia=d2,franjahoraria=f)
                    return(asignatura.franjahoraria.abreviatura)
                    print(asignatura)
     model.F2 = Param(model.DO2, within=model.F, initialize = fran_pos)

     def num_gru(model, i):
        asignatura=AsignaturaPregrado.objects.get(pk=i)
        return(asignatura.numerogrupos)
     model.G = Param(model.I, initialize = num_gru)

     def sec_sem(model, i):
        asignatura=AsignaturaPregrado.objects.get(pk=i)
        return(asignatura.sesionessemanales)
     model.S1 = Param(model.I, initialize = sec_sem)

     #model.G = Param(model.I, initialize = numerogrupos) # numero de grupos a programar de la asignatura i.
     #model.S1 = Param(model.I, initialize = sesionessemanales) # numero de sesiones semanales de la asignatura i.


     model.O = Set(model.I, within=model.Opt1, initialize = kpregrado) # Categoria a la cual pertenece la asignatura i
     
     def cup_req(model, i):
        asignatura=AsignaturaPregrado.objects.get(pk=i)
        return(asignatura.cuporequerido)
     model.Q1 = Param(model.I, initialize = cup_req)
     
     def sem(model, i):
        programa=ProgramaPregrado.objects.get(pk=i)
        return(programa.semestres)
     model.T1 = Param(model.P1, initialize = sem)
     #model.Q1 = Param(model.I, initialize = cuporequerido) # Cupo requerido en numero de estudiantes de la asignatura i
     #model.T1 = Param(model.P1, initialize = semestres ) # numero de semestres del pregrado p.


     def GI_init(model):
          return ((i1,g) for i1 in model.I for g in range(1,model.G[i1]+1))
     model.GI=Set(dimen=2, initialize=GI_init)

     # asignaturasmovilidad = AsignaturaGrupoMovilidad.objects.filter(instancia = ins)
     # asignaturamovilidad = []
     # for B in asignaturasmovilidad:
     #      asignaturamovilidad.append(str(B.movilidad_reducida))

     def asig_pos(model):
          for i1,g in model.GI:
               asignatura=AsignaturaGrupoMovilidad.objects.get(asignaturapregrado=i1,grupo=g)
          return(asignatura.movilidad_reducida)
     model.B = Param(model.GI, initialize = asig_pos)
     
     #model.B = Param(model.GI, initialize = asignaturamovilidad) # 1, si el grupo g de la asignatura i requiere espacios fisicos para personas de movilidad reducida.

     def TP1_init(model):
          return ((p1,t1) for p1 in model.P1 for t1 in range(1,model.T1[p1]+1))
     model.TP1=Set(dimen=2, initialize=TP1_init)

     asignaturaspregrado = PregradoSemestreAsignatura.objects.filter(instancia = ins)
     asignaturapregrado = []
     for A in asignaturaspregrado:
          asignaturapregrado.append(A.asignaturapregrado.abreviatura)

     def sem_pos(model, i):
        programa=ProgramaPosgrado.objects.get(pk=i)
        return(programa.semestres)
     model.T2 = Param(model.P2, initialize = sem_pos)

     model.A = Set(model.TP1, initialize = asignaturapregrado) # asignatura de semestres del pregrado p.
     #model.T2 = Param(model.P2, initialize = semestrespos) # numero de semestres del posgrado p.

     def TP2_init(model):
          return ((p2,t2) for p2 in model.P2 for t2 in range(1,model.T2[p2]+1))
     model.TP2=Set(dimen=2, initialize=TP2_init)

     # posgradosemestres = PosgradoSemestreSesion.objects.filter(instancia = ins)
     # sesionesposgrado = []
     # cupoposgrado = []
     # movilidadposgrado = []
     # for ps in posgradosemestres:
     #      sesionesposgrado.append(ps.sesiones)
     #      cupoposgrado.append(ps.cuporequerido)
     #      movilidadposgrado.append(ps.movilidad_reducida)

     def sec_pos(model):
          for p2,t2 in model.TP2:
               programa=PosgradoSemestreSesion.objects.get(programaposgrado=p2,semestre=t2)
          return(programa.sesiones)
     model.S2 = Param(model.TP2, initialize = sec_pos)


     def cup_pos(model):
          for p2,t2 in model.TP2:
               programa=PosgradoSemestreSesion.objects.get(programaposgrado=p2,semestre=t2)
          return(programa.cuporequerido)
     model.Q2 = Param(model.TP2, initialize = cup_pos)

     def mov_pos(model):
          for p2,t2 in model.TP2:
               programa=PosgradoSemestreSesion.objects.get(programaposgrado=p2,semestre=t2)
          return(programa.movilidad_reducida)
     model.H = Param(model.TP2, initialize = mov_pos)
     #model.S2 = Param(model.TP2, initialize = sesionesposgrado) # numero de sesiones a programar del semestre t del posgrado p.
     #model.Q2 = Param(model.TP2, initialize = cupoposgrado)# Cupo requerido en numero de estudiantes del semestre t del posgrado p.
     #model.H = Param(model.TP2, initialize = movilidadposgrado) # 1, si el semestre t del posgrado p requiere espacios fisicos para personas de movilidad reducida.



     # Variable initialization #NonNegativeReals

     def IGSODRF_init(model):
          return ((i,g,s1,l,d,r,f) for i in model.I for g in range(1,model.G[i]+1) for s1 in range(1,model.S1[i]+1) for l in model.O[i] for d in model.D1[l] for r in model.R1[l] for f in model.F1[l,d] if ((model.C[r]-(model.Q1[i]/model.G[i]))>=0) if (model.B[i,g]<=model.Z[r]))
     model.IGSODRF=Set(dimen=7, initialize=IGSODRF_init)

     model.X = Var(model.IGSODRF, within=Binary)

     def INFACTX_init(model):
          return ((i,g) for i in model.I for g in range(1,model.G[i]+1))
     model.INFACTX=Set(dimen=2, initialize=INFACTX_init)

     model.NX = Var(model.INFACTX, within=Binary)


     def PKTSDRF_init(model):
          return ((p2,v,t2,s2,d,r,f) for p2 in model.P2 for v in model.K2[p2] for t2 in range(1,model.T2[p2]+1) for s2 in range(1,model.S2[p2,t2]+1) for d in model.D2[v] for r in model.R2[v] for f in model.F2[v,d] if (model.C[r]-model.Q2[p2,t2]>=0) if (model.H[p2,t2]<=model.Z[r]) )
     model.PKTSDRF=Set(dimen=7, initialize=PKTSDRF_init)

     model.Y = Var(model.PKTSDRF, within=Binary)

     def INFACTY_init(model):
          return ((p2,t2,s2) for p2 in model.P2 for t2 in range(1,model.T2[p2]+1) for s2 in range(1,model.S2[p2,t2]+1))
     model.INFACTY=Set(dimen=3, initialize=INFACTY_init)

     model.NY = Var(model.INFACTY, within=Binary)

     # Funcion objetivo
     def slack(model):
          return (sum(model.X[i,g,s1,l,d,r,f]*(model.C[r]-(model.Q1[i]/model.G[i])) for (i,g,s1,l,d,r,f) in model.IGSODRF )+sum(model.Y[p2,v,t2,s2,d,r,f]*(model.C[r]-model.Q2[p2,t2]) for (p2,v,t2,s2,d,r,f) in model.PKTSDRF) + 100000*sum(model.NY[p2,t2,s2] for (p2,t2,s2) in model.INFACTY) + 10000000*sum(model.NX[i,g] for (i,g) in model.INFACTX))
     model.Slack = Objective(rule=slack, sense=minimize)

     # Restricciones

     #if ((model.C[r]-(model.Q1[i]/model.G[i]))>=0) if (model.B[i,g]<=model.Z[r])
     #if (model.C[r]-model.Q2[p2,t2]>=0) if (model.H[p2,t2]<=model.Z[r])

     # 1. (oblig) Satisfaccion demanda de salones de pregrado
     def R1a_init(model):
          return ((i,g,s1,l) for i in model.I for g in range(1,model.G[i]+1) for s1 in range(1,model.S1[i]+1) for l in model.O[i])
     model.R1a=Set(dimen=4, initialize=R1a_init)

     def R1a(model,i,g,s1,l):
          return sum(sum(sum(model.X[i,g,s1,l,d,r,f] for f in model.F1[l,d] if ((model.C[r]-(model.Q1[i]/model.G[i]))>=0) if (model.B[i,g]<=model.Z[r])) for r in model.R1[l]) for d in model.D1[l]) + model.NX[i,g]  == 1
     model.r1 = Constraint(model.R1a,rule=R1a)


     # 2. (oblig) Satisfaccion demanda de salones de posgrado
     def R2a_init(model):
          return ((p2,v,t2,s2) for p2 in model.P2 for v in model.K2[p2] for t2 in range(1,model.T2[p2]+1) for s2 in range(1,model.S2[p2,t2]+1) )
     model.R2a=Set(dimen=4, initialize=R2a_init)

     def R2b(model,p2,v,t2,s2):
          return sum(sum(sum(model.Y[p2,v,t2,s2,d,r,f] for f in model.F2[v,d] if (model.C[r]-model.Q2[p2,t2]>=0) if (model.H[p2,t2]<=model.Z[r])) for r in model.R2[v]) for d in model.D2[v]) + model.NY[p2,t2,s2] == 1
     model.r2 = Constraint(model.R2a,rule=R2b)

     # 3. (oblig) No sesiones simultaneas en espacio fisico
     def R3a_init(model):
          return ((d,r,f) for i in model.I for g in range(1,model.G[i]+1) for l in model.O[i] for d in model.D1[l] for r in model.R1[l] for f in model.F1[l,d] if ((model.C[r]-(model.Q1[i]/model.G[i]))>=0) if (model.B[i,g]<=model.Z[r]))
     model.R3a=Set(dimen=3, initialize=R3a_init)

     def R3c(model,d,r,f):
          return (sum(sum(sum(sum(model.X[i,g,s1,l,d,r,f] for s1 in range(1,model.S1[i]+1) if (model.C[r]-(model.Q1[i]/model.G[i])>=0) if (model.B[i,g]<=model.Z[r]) if d in model.D1[l] if f in model.F1[l,d]  if r in model.R1[l]) for l in model.O[i]) for g  in range(1,model.G[i]+1) )  for i  in model.I)) <= 1
     model.r3c = Constraint(model.R3a,rule=R3c)

     def R3b_init(model):
          return ((d,r,f) for p2 in model.P2 for v in model.K2[p2] for t2 in range(1,model.T2[p2]+1) for d in model.D2[v] for r in model.R2[v] for f in model.F2[v,d] if (model.C[r]-model.Q2[p2,t2]>=0) if (model.H[p2,t2]<=model.Z[r]) )
     model.R3b=Set(dimen=3, initialize=R3b_init)

     def R3d(model,d,r,f):
          return (sum(sum(sum(sum(model.Y[p2,v,t2,s2,d,r,f] for s2 in range(1,model.S2[p2,t2]+1) if (model.C[r]-model.Q2[p2,t2]>=0) if (model.H[p2,t2]<=model.Z[r]) if d in model.D2[v] if r in model.R2[v] if f in model.F2[v,d] ) for v in model.K2[p2] ) for t2 in range(1,model.T2[p2]+1)) for p2 in model.P2)) <= 1
     model.r3d = Constraint(model.R3b,rule=R3d)

     def R3e(model,d,r,f):
          return (sum(sum(sum(sum(model.Y[p2,v,t2,s2,d,r,f] for s2 in range(1,model.S2[p2,t2]+1) if (model.C[r]-model.Q2[p2,t2]>=0) if (model.H[p2,t2]<=model.Z[r]) if d in model.D2[v] if r in model.R2[v] if f in model.F2[v,d] ) for v in model.K2[p2] ) for t2 in range(1,model.T2[p2]+1)) for p2 in model.P2) + sum(sum(sum(sum(model.X[i,g,s1,l,d,r,f] for s1 in range(1,model.S1[i]+1) if (model.C[r]-(model.Q1[i]/model.G[i])>=0) if (model.B[i,g]<=model.Z[r]) if d in model.D1[l] if f in model.F1[l,d] if r in model.R1[l]) for l in model.O[i]) for g  in range(1,model.G[i]+1) )  for i  in model.I)) <= 1
     model.r3e = Constraint(model.R3a.intersection(model.R3b),rule=R3e)
     
     # 4. (oblig) No cruce de asignaturas de mismo semestre posgrado
     def R4a_init(model):
          return ((p2,v,t2,d,f) for p2 in model.P2 for v in model.K2[p2] for t2 in range(1,model.T2[p2]+1) for d in model.D2[v] for f in model.F2[v,d])
     model.R4a=Set(dimen=5, initialize=R4a_init)

     def R4(model,p2,v,t2,d,f):
          return sum(sum(model.Y[p2,v,t2,s2,d,r,f] for s2 in range(1,model.S2[p2,t2]+1) if (model.C[r]-model.Q2[p2,t2]>=0) if (model.H[p2,t2]<=model.Z[r])) for r in model.R2[v]) <= 1
     model.r4 = Constraint(model.R4a,rule=R4)

     # 5. (oblig) No sesiones de asignaturas de mismo semestre pregrado
     def R5a_init(model):
          return ((p,t,i,l1,j,l2,g1,g2,s1,s2,f,d) 
               for p in model.P1 for t in range(1,model.T1[p]+1) 
               for i in model.A[p,t] for l1 in model.O[i] for g1 in range(1,model.G[i]+1) 
               for j in model.A[p,t] for l2 in model.O[j] for g2 in range(1,model.G[j]+1)
               for s1 in range(1,model.S1[i]+1) for s2 in range(1,model.S1[j]+1) 
               for f in model.F for d in model.D if i!=j 
               if len(model.A[p,t])>0 if d in model.D1[l1] if d in model.D1[l2]
               if f in model.F1[l1,d] if f in model.F1[l2,d])
     model.R5a=Set(dimen=12, initialize=R5a_init)


     def R5(model,p,t,i,l1,j,l2,g1,g2,s1,s2,f,d):
          return (sum(model.X[i,g1,s1,l1,d,r1,f] for r1 in model.R1[l1] if ((model.C[r1]-(model.Q1[i]/model.G[i]))>=0) if (model.B[i,g1]<=model.Z[r1])) +
               sum(model.X[j,g2,s2,l2,d,r2,f] for r2 in model.R1[l2] if ((model.C[r2]-(model.Q1[j]/model.G[j]))>=0) if (model.B[j,g2]<=model.Z[r2])))<= 1
     model.r5 = Constraint(model.R5a,rule=R5)


     # 6. (oblig) No cruce de sesiones de misma asignatura en mismo dia
     def R6a_init(model):
          return ((i,l,g,d) for i in model.I for l in model.O[i] for g in range(1,model.G[i]+1) for d in model.D1[l] if model.S1[i]==2)
     model.R6a=Set(dimen=4, initialize=R6a_init)

     def R6(model,i,l,g,d):
          return sum(sum(model.X[i,g,1,l,d,r,f]+model.X[i,g,2,l,d,r,f] for f in model.F1[l,d] if ((model.C[r]-(model.Q1[i]/model.G[i]))>=0) if (model.B[i,g]<=model.Z[r])) for r in model.R1[l])  <= 1
     model.r6 = Constraint(model.R6a,rule=R6)

     # 7. (oblig) Asignacion de sesiones de posgrado para el sabado    
     def R7a_init(model):
          return ((p2,v,t2,r) for p2 in model.P2 for v in model.K2[p2] for t2 in range(1,model.T2[p2]+1) for r in model.R2[v] if (model.C[r]-model.Q2[p2,t2]>=0) if (model.H[p2,t2]<=model.Z[r]) if ('Sab' in model.D2[v]) if ('0700' in model.F2[v,'Sab']) if ('1000' in model.F2[v,'Sab']))
     model.R7a=Set(dimen=4, initialize=R7a_init)

     def R7(model,p2,v,t2,r):
          return  (sum(model.Y[p2,v,t2,s1,'Sab',r,'0700'] for s1 in range(1,model.S2[p2,t2]+1)) - sum(model.Y[p2,v,t2,s2,'Sab',r,'1000'] for s2 in range(1,model.S2[p2,t2]+1)) == 0 )
     model.r7 = Constraint(model.R7a,rule=R7)

     # 8. (semi) Limite de sesiones-asignatura-programa dia
     def R8a_init(model):
          return ((tp1,d) for tp1 in model.TP1 for i in model.A[tp1] for l in model.O[i] for d in model.D1[l] if len(model.A[tp1])>0)
     model.R8a=Set(dimen=3, initialize=R8a_init)

     def R8(model,p,t,d):
          return sum(sum(sum(sum(sum(sum(model.X[i,g,s1,l,d,r,f] for f in model.F1[l,d] if ((model.C[r]-(model.Q1[i]/model.G[i]))>=0) if (model.B[i,g]<=model.Z[r])) for r in model.R1[l]) for l in model.O[i] if (d in model.D1[l])) for s1 in range(1,model.S1[i]+1)) for g in range(1,model.G[i]+1)) for i in model.A[p,t]) <=3
     model.r8 = Constraint(model.R8a,rule=R8)

     # 9. (sug) Un dia de espacio entre sesiones de una misma asignatura
     def R9a_init(model):
          return ((q,i,l,g,d) for i in model.I for l in model.O[i] for g in range(1,model.G[i]+1) for d in model.D1[l] for q in model.W[d] if model.S1[i]==2 if q in model.D1[l] if d!="Sab")
     model.R9a=Set(dimen=5, initialize=R9a_init)

     def R9b(model,q,i,l,g,d):
          return sum(sum(model.X[i,g,1,l,d,r,f] for f in model.F1[l,d] if f in model.F1[l,d] if ((model.C[r]-(model.Q1[i]/model.G[i]))>=0) if (model.B[i,g]<=model.Z[r])) for r in model.R1[l])+sum(sum(model.X[i,g,2,l,q,r,f] for f in model.F1[l,q] if f in model.F1[l,q] if ((model.C[r]-(model.Q1[i]/model.G[i]))>=0) if (model.B[i,g]<=model.Z[r])) for r in model.R1[l])  <= 1
     model.r9b = Constraint(model.R9a,rule=R9b)

     def R9c(model,q,i,l,g,d):
          return sum(sum(model.X[i,g,2,l,d,r,f] for f in model.F1[l,d] if f in model.F1[l,d] if ((model.C[r]-(model.Q1[i]/model.G[i]))>=0) if (model.B[i,g]<=model.Z[r])) for r in model.R1[l])+sum(sum(model.X[i,g,1,l,q,r,f] for f in model.F1[l,q] if f in model.F1[l,q] if ((model.C[r]-(model.Q1[i]/model.G[i]))>=0) if (model.B[i,g]<=model.Z[r])) for r in model.R1[l])  <= 1
     model.r9c = Constraint(model.R9a,rule=R9c)

     # 10. (semi) calidad sabados     
     def SAB07a_init(model, p1, t1, r):
          return ((i,g,s1,l,d,f) for i in model.A[p1,t1] for g in range(1,model.G[i]+1) for s1 in range(1,model.S1[i]+1) for l in model.O[i] for d in model.D1[l] for r in model.R1[l] for f in model.F1[l,d] if ((model.C[r]-(model.Q1[i]/model.G[i]))>=0) if (model.B[i,g]<=model.Z[r]) if (d=='Sab') if (f=='1000') if ('0700' in model.F1[l,d]))
     model.SAB07a = Set(model.TP1,model.R, initialize=SAB07a_init, dimen=6)

     def SAB10a_init(model, p1, t1, r):
          return ((i,g,s1,l,d,f) for i in model.A[p1,t1] for g in range(1,model.G[i]+1) for s1 in range(1,model.S1[i]+1) for l in model.O[i] for d in model.D1[l] for r in model.R1[l] for f in model.F1[l,d] if ((model.C[r]-(model.Q1[i]/model.G[i]))>=0) if (model.B[i,g]<=model.Z[r]) if (d=='Sab') if (f=='0700') if ('10000' in model.F1[l,d]))
     model.SAB10a = Set(model.TP1, model.R, initialize=SAB10a_init, dimen=6)  

     def TP1a_init(model):
          return ((p1,t1,r) for (p1,t1) in model.TP1 for r in model.R if len(model.SAB07a[p1,t1,r])>0 if len(model.SAB10a[p1,t1,r])>0)
     model.TP1a = Set(dimen=3, initialize=TP1a_init)

     def R10a(model,p1,t1,r):
          return (sum(model.X[i,g,s1,l,d,r,f] for (i,g,s1,l,d,f) in model.SAB07a[p1,t1,r])-sum(model.X[i,g,s1,l,d,r,f] for (i,g,s1,l,d,f) in model.SAB10a[p1,t1,r])==0)
     model.r10a = Constraint(model.TP1a,rule=R10a)

     def TP1b_init(model):
          return ((i,g,l,d,r,f1,f2) for i in model.I for l in model.O[i] for g in range(1,model.G[i]+1) for d in model.D1[l] for r in model.R1[l] for f1 in model.F1[l,d] for f2 in model.F1[l,d] if (d=='Sab') if (f1=='0700') if (f2=='1000') if (model.S1[i]==2)  if ((model.C[r]-(model.Q1[i]/model.G[i]))>=0) if (model.B[i,g]<=model.Z[r]))
     model.TP1b = Set(dimen=7, initialize=TP1b_init)

     def R10b(model,i,g,l,d,r,f1,f2):
          return (model.X[i,g,1,l,d,r,f1] - model.X[i,g,2,l,d,r,f2] == 0)
     model.r10b = Constraint(model.TP1b,rule=R10b)

     # 11. (sug) zoom
     def R11a_init(model):
          return ((d,f) for i in model.I for g in range(1,model.G[i]+1) for l in model.O[i] for d in model.D1[l] for f in model.F1[l,d] )
     model.R11a=Set(dimen=2, initialize=R11a_init)

     def R11c(model,d,f):
          return (sum(sum(sum(sum(sum(model.X[i,g,s1,l,d,r,f] for s1 in range(1,model.S1[i]+1) if (model.C[r]-(model.Q1[i]/model.G[i])>=0) if (model.B[i,g]<=model.Z[r]) if d in model.D1[l] if f in model.F1[l,d]  if r in model.R1[l]) for r in model.R1[l]) for l in model.O[i]) for g  in range(1,model.G[i]+1) )  for i  in model.I)) <= model.ZOOM
     model.r11c = Constraint(model.R11a,rule=R11c)

     def R11b_init(model):
          return ((d,f) for p2 in model.P2 for v in model.K2[p2] for t2 in range(1,model.T2[p2]+1) for d in model.D2[v] for f in model.F2[v,d] )
     model.R11b=Set(dimen=2, initialize=R11b_init)

     def R11d(model,d,f):
          return (sum(sum(sum(sum(sum(model.Y[p2,v,t2,s2,d,r,f] for s2 in range(1,model.S2[p2,t2]+1) if (model.C[r]-model.Q2[p2,t2]>=0) if (model.H[p2,t2]<=model.Z[r]) if d in model.D2[v] if r in model.R2[v] if f in model.F2[v,d] ) for r in model.R2[v]) for v in model.K2[p2] )  for t2 in range(1,model.T2[p2]+1)) for p2 in model.P2)) <= model.ZOOM
     model.r11d = Constraint(model.R11b,rule=R11d)

     def R11e(model,d,f):
          return (sum(sum(sum(sum(sum(model.Y[p2,v,t2,s2,d,r,f] for s2 in range(1,model.S2[p2,t2]+1) if (model.C[r]-model.Q2[p2,t2]>=0) if (model.H[p2,t2]<=model.Z[r]) if d in model.D2[v] if r in model.R2[v] if f in model.F2[v,d] ) for r in model.R2[v]) for v in model.K2[p2] ) for t2 in range(1,model.T2[p2]+1)) for p2 in model.P2) + sum(sum(sum(sum(sum(model.X[i,g,s1,l,d,r,f] for s1 in range(1,model.S1[i]+1) if (model.C[r]-(model.Q1[i]/model.G[i])>=0) if (model.B[i,g]<=model.Z[r]) if d in model.D1[l] if f in model.F1[l,d] if r in model.R1[l]) for r in model.R1[l]) for l in model.O[i]) for g  in range(1,model.G[i]+1) )  for i  in model.I)) <= model.ZOOM
     model.r11e = Constraint(model.R11a.intersection(model.R11b),rule=R11e)

     # 13. (sug) un dia entre sesiones de posgrado
     def R13a_init(model):
          return ((p2,v,t2,d,w) for p2 in model.P2 for v in model.K2[p2] for t2 in range(1,model.T2[p2]+1) for d in model.D2[v] for w in model.W[d] if len(model.W[d])>0 if (d!='Sab') if (d!='Vie'))
     model.R13a=Set(dimen=5, initialize=R13a_init)

     def R13(model,p2,v,t2,d,w):
          return sum(sum(sum((model.Y[p2,v,t2,s2,d,r,f]+model.Y[p2,v,t2,s2,w,r,f]) for f in model.F2[v,d]) for s2 in range(1,model.S2[p2,t2]+1) if (model.C[r]-model.Q2[p2,t2]>=0) if (model.H[p2,t2]<=model.Z[r])) for r in model.R2[v]) <= 1
     model.r13 = Constraint(model.R13a,rule=R13)

     # 14. (oblig) No cruce de sesiones de asignaturas de mismo profesor
     def R14a_init(model):
          return ((k,i,l1,j,l2,g1,g2,s1,s2,f,d) 
               for k in model.teacher 
               for i in model.AsigTeach[k] for l1 in model.O[i] for g1 in range(1,model.G[i]+1) 
               for j in  model.AsigTeach[k] for l2 in model.O[j] for g2 in range(1,model.G[j]+1)
               for s1 in range(1,model.S1[i]+1) for s2 in range(1,model.S1[j]+1) 
               for f in model.F for d in model.D if i!=j 
               if d in model.D1[l1] if d in model.D1[l2] if len(model.AsigTeach[k])>0
               if f in model.F1[l1,d] if f in model.F1[l2,d])
     model.R14a=Set(dimen=11, initialize=R14a_init)


     def R14(model,k,i,l1,j,l2,g1,g2,s1,s2,f,d):
          return (sum(model.X[i,g1,s1,l1,d,r1,f] for r1 in model.R1[l1] if ((model.C[r1]-(model.Q1[i]/model.G[i]))>=0) if (model.B[i,g1]<=model.Z[r1])) +
               sum(model.X[j,g2,s2,l2,d,r2,f] for r2 in model.R1[l2] if ((model.C[r2]-(model.Q1[j]/model.G[j]))>=0) if (model.B[j,g2]<=model.Z[r2])))<= 1
     model.r14 = Constraint(model.R14a,rule=R14)

     # 15. (oblig) profesores directivas
     def R15a_init(model):
          return ((k,i,l1,g1,s1,f) 
               for k in model.direct 
               for i in model.AsigTeach[k] for l1 in model.O[i] for g1 in range(1,model.G[i]+1) 
               for s1 in range(1,model.S1[i]+1) for f in model.F 
               if len(model.AsigTeach[k])>0 if f in model.F1[l1,d])
     model.R15a=Set(dimen=6, initialize=R15a_init)


     def R15(model,k,i,l1,g1,s1,f):
          return (sum(model.X[i,g1,s1,l1,'Lun',r1,f] for r1 in model.R1[l1] if ((model.C[r1]-(model.Q1[i]/model.G[i]))>=0) if (model.B[i,g1]<=model.Z[r1]))) == 0
     model.r15 = Constraint(model.R15a,rule=R15)


     # 12. (Oblig) variables binarias
     def R12a(model,i,g,s1,l,d,r,f):
          return model.X[i,g,s1,l,d,r,f] <=1
     model.r12a = Constraint(model.IGSODRF,rule=R12a)

     def R12b(model,p2,v,t2,s2,d,r,f):
          return model.Y[p2,v,t2,s2,d,r,f] <=1
     model.r12b = Constraint(model.PKTSDRF,rule=R12b)

     print('vamo por aquí')
     opt = SolverFactory('cplex')
     #opt = SolverFactory('glpk')
     instance = model.create_instance()
     
     #print(instance.cordcliy._data)
     #print(instance.dstcltn._data)
     results = opt.solve(instance)
     instance.X.display()
     print('Esto corrio') 
     instance.pprint()