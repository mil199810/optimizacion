from django.contrib.auth.models import User
from django.db import models
from django.urls import reverse
# Create your models here.

class Instancia(models.Model):
    id = models.AutoField(primary_key = True)
    descripcion = models.TextField('descripcion', max_length=100)
    user = models.ForeignKey(User, on_delete = models.CASCADE)

    def __str__(self):
        return str(self.id)


# Dias programables
class Dia(models.Model): 
    id = models.IntegerField('id')
    abreviatura = models.CharField('abreviatura',primary_key = True,max_length=100, unique=False)
    descripcion = models.TextField('descripcion', max_length=100)
    instancia = models.ForeignKey(Instancia, on_delete = models.CASCADE, blank=True, null=True)


    def __str__(self):
        return str(self.descripcion)



# Categoria Pregrado
class CategoriaPregrado(models.Model): 
    id = models.IntegerField('id')
    abreviatura = models.CharField('abreviatura',primary_key = True,max_length=100, unique=False)
    descripcion = models.TextField('descripcion', max_length=100)
    instancia = models.ForeignKey(Instancia, on_delete = models.CASCADE, blank=True, null=True)


    def __str__(self):
        return str(self.abreviatura)


# Categoria Posgrado
class CategoriaPosgrado(models.Model): 
    id = models.IntegerField('id')
    abreviatura = models.CharField('abreviatura',primary_key = True,max_length=100, unique=False)
    descripcion = models.TextField('descripcion', max_length=100)
    instancia = models.ForeignKey(Instancia, on_delete = models.CASCADE, blank=True, null=True)


    def __str__(self):
        return str(self.abreviatura)

# Asignaturas de pregrado
class AsignaturaPregrado(models.Model): 
    id = models.IntegerField('id')
    abreviatura = models.CharField('abreviatura',primary_key = True,max_length=100, unique=False)
    descripcion = models.TextField('descripcion', max_length=100)
    numerogrupos = models.IntegerField('numerogrupos') # numero de grupos a programar de la asignatura i.
    sesionessemanales = models.IntegerField('sesionessemanales') # numero de sesiones semanales de la asignatura i.
    cuporequerido = models.IntegerField('cuporequerido') # Cupo requerido en numero de estudiantes de la asignatura i
    categoriapregrado = models.ForeignKey(CategoriaPregrado, on_delete = models.CASCADE, blank=True, null=True) # Categoria a la cual pertenece la asignatura i
    instancia = models.ForeignKey(Instancia, on_delete = models.CASCADE, blank=True, null=True)


    def __str__(self):
        return str(self.abreviatura)


# Salones programables
class Salon(models.Model): 
    id = models.IntegerField('id')
    abreviatura = models.CharField('abreviatura',primary_key = True,max_length=100, unique=False)
    descripcion = models.TextField('descripcion', max_length=100)
    capacidad = models.IntegerField('capacidad')
    movilidad_reducida = models.IntegerField('movilidad_reducida')
    instancia = models.ForeignKey(Instancia, on_delete = models.CASCADE, blank=True, null=True)


    def __str__(self):
        return str(self.descripcion)


# Franjas horarias
class FranjaHoraria(models.Model): 
    id = models.IntegerField('id')
    abreviatura = models.CharField('abreviatura',primary_key = True,max_length=100)
    descripcion = models.TextField('descripcion', max_length=100)
    instancia = models.ForeignKey(Instancia, on_delete = models.CASCADE, blank=True, null=True)


    def __str__(self):
        return str(self.descripcion)

# Programas de pregrado
class ProgramaPregrado(models.Model): 
    id = models.IntegerField('id')
    abreviatura = models.CharField('abreviatura',primary_key = True,max_length=100)
    descripcion = models.TextField('descripcion', max_length=100)
    semestres = models.IntegerField('semestres')  # numero de semestres del pregrado p.
    instancia = models.ForeignKey(Instancia, on_delete = models.CASCADE, blank=True, null=True)


    def __str__(self):
        return str(self.descripcion)

# Programas de posgrado
class ProgramaPosgrado(models.Model): 
    id = models.IntegerField('id')
    abreviatura = models.CharField('abreviatura',primary_key = True,max_length=100)
    descripcion = models.TextField('descripcion', max_length=100)
    semestres = models.IntegerField('semestres')  # numero de semestres del posgrado p.
    categoriaposgrado = models.ForeignKey(CategoriaPosgrado, on_delete = models.CASCADE, blank=True, null=True) # categoria al que pertenece el posgrado p2
    instancia = models.ForeignKey(Instancia, on_delete = models.CASCADE, blank=True, null=True)


    def __str__(self):
        return str(self.descripcion)

# Profesores
class Profesor(models.Model):
    cedula = models.IntegerField(primary_key = True)
    nombre = models.TextField('nombres', max_length=100)
    instancia = models.ForeignKey(Instancia, on_delete = models.CASCADE, blank=True, null=True)


    def __str__(self):
        return str(self.nombre)


# Profesores
class ProfesorDirectivo(models.Model):
    id = models.AutoField(primary_key = True)
    profesor = models.ForeignKey(Profesor, on_delete = models.CASCADE, blank=True, null=True)
    instancia = models.ForeignKey(Instancia, on_delete = models.CASCADE, blank=True, null=True)


    def __str__(self):
        return str(self.profesor)

# Profesores
class AsignaturaProfesor(models.Model):
    id = models.AutoField(primary_key = True)
    profesor = models.ForeignKey(Profesor, on_delete = models.CASCADE, blank=True, null=True)
    asignaturapregrado = models.ForeignKey(AsignaturaPregrado, on_delete = models.CASCADE, blank=True, null=True)
    instancia = models.ForeignKey(Instancia, on_delete = models.CASCADE, blank=True, null=True)


    def __str__(self):
        return str(self.profesor)+ "-" + str(self.asignaturapregrado)


# Franjas disponibles para la categoria pregrado o1
class FranjaPregrado(models.Model):
    id = models.AutoField(primary_key = True)
    categoriapregrado = models.ForeignKey(CategoriaPregrado, on_delete = models.CASCADE, blank=True, null=True)
    dia = models.ForeignKey(Dia, on_delete = models.CASCADE, blank=True, null=True)
    franjahoraria = models.ForeignKey(FranjaHoraria, on_delete = models.CASCADE, blank=True, null=True)
    instancia = models.ForeignKey(Instancia, on_delete = models.CASCADE, blank=True, null=True)


    def __str__(self):
        return str(self.categoriapregrado) + "-" + str(self.dia)+ "-" + str(self.franjahoraria)

# Franjas disponibles para la categoria posgrado o2
class FranjaPosgrado(models.Model):
    id = models.AutoField(primary_key = True)
    categoriaposgrado = models.ForeignKey(CategoriaPosgrado, on_delete = models.CASCADE, blank=True, null=True)
    dia = models.ForeignKey(Dia, on_delete = models.CASCADE, blank=True, null=True)
    franjahoraria = models.ForeignKey(FranjaHoraria, on_delete = models.CASCADE, blank=True, null=True)
    instancia = models.ForeignKey(Instancia, on_delete = models.CASCADE, blank=True, null=True)


    def __str__(self):
        return str(self.categoriaposgrado) + "-" + str(self.dia)+ "-" + str(self.franjahoraria)


# Salones de clase disponibles para la categoria pregado o1
class SalonPregrado(models.Model):
    id = models.AutoField(primary_key = True)
    categoriapregrado = models.ForeignKey(CategoriaPregrado, on_delete = models.CASCADE, blank=True, null=True)
    salon = models.ForeignKey(Salon, on_delete = models.CASCADE, blank=True, null=True)
    instancia = models.ForeignKey(Instancia, on_delete = models.CASCADE, blank=True, null=True)


    def __str__(self):
        return str(self.categoriapregrado) + "-" + str(self.salon) 


# Dias de clase disponibles para la categoria pregado o1
class DiaPregrado(models.Model):
    id = models.AutoField(primary_key = True)
    categoriapregrado = models.ForeignKey(CategoriaPregrado, on_delete = models.CASCADE, blank=True, null=True)
    dia = models.ForeignKey(Dia, on_delete = models.CASCADE, blank=True, null=True)
    instancia = models.ForeignKey(Instancia, on_delete = models.CASCADE, blank=True, null=True)


    def __str__(self):
        return str(self.categoriapregrado) + "-" + str(self.dia) 


# Salones de clase disponibles para la categoria posgrado o2
class SalonPosgrado(models.Model):
    id = models.AutoField(primary_key = True)
    categoriaposgrado = models.ForeignKey(CategoriaPosgrado, on_delete = models.CASCADE, blank=True, null=True)
    salon = models.ForeignKey(Salon, on_delete = models.CASCADE, blank=True, null=True)
    instancia = models.ForeignKey(Instancia, on_delete = models.CASCADE, blank=True, null=True)


    def __str__(self):
        return str(self.categoriaposgrado) + "-" + str(self.salon)




# Dias de clase disponibles para la categoria posgrado o2
class DiaPosgrado(models.Model):
    id = models.AutoField(primary_key = True)
    categoriaposgrado = models.ForeignKey(CategoriaPosgrado, on_delete = models.CASCADE, blank=True, null=True)
    dia = models.ForeignKey(Dia, on_delete = models.CASCADE, blank=True, null=True)
    instancia = models.ForeignKey(Instancia, on_delete = models.CASCADE, blank=True, null=True)


    def __str__(self):
        return str(self.categoriaposgrado) + "-" + str(self.dia)



class W(models.Model):
    id = models.AutoField(primary_key = True)
    dia = models.ForeignKey(Dia, on_delete = models.CASCADE, blank=True, null=True,related_name='dia')
    dia1 = models.ForeignKey(Dia, on_delete = models.CASCADE, blank=True, null=True, related_name='dia1')
    instancia = models.ForeignKey(Instancia, on_delete = models.CASCADE, blank=True, null=True)

    def __str__(self):
        return str(self.dia) + "-" + str(self.dia1)


# asignatura de semestres del pregrado p.
class PregradoSemestreAsignatura(models.Model):
    id = models.AutoField(primary_key = True)
    programapregado = models.ForeignKey(ProgramaPregrado, on_delete = models.CASCADE, blank=True, null=True)
    semestre = models.IntegerField('semestre') 
    asignaturapregrado = models.ForeignKey(AsignaturaPregrado, on_delete = models.CASCADE, blank=True, null=True)
    instancia = models.ForeignKey(Instancia, on_delete = models.CASCADE, blank=True, null=True)

    def __str__(self):
        return str(self.programapregado) + "-" + str(self.semestre)+ "-" + str(self.asignaturapregrado)


 # 1, si el grupo g de la asignatura i requiere espacios fisicos para personas de movilidad reducida.
class AsignaturaGrupoMovilidad(models.Model):
    id = models.AutoField(primary_key = True)
    asignaturapregrado = models.ForeignKey(AsignaturaPregrado, on_delete = models.CASCADE, blank=True, null=True)
    grupo = models.IntegerField('grupo')  
    movilidad_reducida = models.IntegerField('movilidad_reducida')  
    instancia = models.ForeignKey(Instancia, on_delete = models.CASCADE, blank=True, null=True)

    def __str__(self):
        return str(self.asignaturapregrado) + "-" + str(self.grupo)+ "-" + str(self.movilidad_reducida)


# numero de sesiones a programar del semestre t del posgrado p.
class PosgradoSemestreSesion(models.Model):
    id = models.AutoField(primary_key = True)
    programaposgrado = models.ForeignKey(ProgramaPosgrado, on_delete = models.CASCADE, blank=True, null=True)
    semestre = models.IntegerField('semestre') 
    sesiones = models.IntegerField('sesiones')
    cuporequerido = models.IntegerField('cuporequerido') # Cupo requerido en numero de estudiantes del semestre t del posgrado p.
    movilidad_reducida = models.IntegerField('movilidad_reducida')  
    instancia = models.ForeignKey(Instancia, on_delete = models.CASCADE, blank=True, null=True)


    def __str__(self):
        return str(self.programaposgrado) + "-" + str(self.semestre)+ "-" + str(self.sesiones)


class Resultados(models.Model):
    id = models.IntegerField('id', primary_key=True)
    asignaturapregrado = models.ForeignKey(AsignaturaPregrado, on_delete = models.CASCADE, blank=True, null=True)
    grupo = models.IntegerField('grupo', null=True) # numero de grupos a programar de la asignatura i.
    sesiones = models.IntegerField('sesiones', null=True) # numero de sesiones semanales de la asignatura i.
    categoriapregrado = models.ForeignKey(CategoriaPregrado, on_delete = models.CASCADE, blank=True, null=True) # Categoria a la cual pertenece la asignatura i
    dia = models.ForeignKey(Dia, on_delete = models.CASCADE, blank=True, null=True)
    salon = models.ForeignKey(Salon, on_delete = models.CASCADE, blank=True, null=True)
    franjahoraria = models.ForeignKey(FranjaHoraria, on_delete = models.CASCADE, blank=True, null=True)
    
    def __str__(self):
        return str(self.asignaturapregrado) + "-" + str(self.grupo)+ "-" + str(self.sesiones)+ "-" + str(self.categoriapregrado)+ "-" + str(self.dia)+ "-" + str(self.salon)+ "-" + str(self.franjahoraria)