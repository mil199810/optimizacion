from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from .utils import optimizar
from .models import Salon, Resultados,Dia,FranjaHoraria,ProgramaPregrado, PregradoSemestreAsignatura, AsignaturaPregrado
# Create your views here.


def home(request):
    #instance.pprint()
    #instance.Y.display()
    #optimizar(1)
    contexto={}
    salones = Salon.objects.filter()
    programas = ProgramaPregrado.objects.filter()
    contexto['programas'] = programas
    contexto['salones']=salones

    return render(request, 'salones/index.html', contexto)


def scheduling(request, pk_salon):
    contexto={}
    franjas = FranjaHoraria.objects.filter()
    dias = Dia.objects.filter()
    existe = {}
    programacion = {}
    asignaturas = {}
    grupos = {}
    sesiones = {}
    for dia in dias:
        for franja in franjas:
            if(Resultados.objects.filter(dia=dia,franjahoraria=franja,salon=pk_salon).exists()):
                salon_dia_franja = Resultados.objects.get(dia=dia,franjahoraria=franja,salon=pk_salon)
                #programacion[str(dia.abreviatura)+"-"+str(franja.abreviatura)]=salon_dia_franja
                asignaturas[str(dia.abreviatura)+"-"+str(franja.abreviatura)]=salon_dia_franja.asignaturapregrado
                grupos[str(dia.abreviatura)+"-"+str(franja.abreviatura)]=salon_dia_franja.grupo
                sesiones[str(dia.abreviatura)+"-"+str(franja.abreviatura)]=salon_dia_franja.sesiones
                existe[str(dia.abreviatura)+"-"+str(franja.abreviatura)]=1


    contexto['franjas'] = franjas
    contexto['dias'] = dias
    contexto['asignaturas'] = asignaturas
    contexto['grupos'] =  grupos
    contexto['sesiones'] = sesiones
    contexto['existe'] = existe
    contexto['pk_salon'] = pk_salon
    print('atras bueno',programacion)
    
    return render(request, 'salones/sheduling.html', contexto)


def scheduling_pro_sem(request, pk_pro, pk_sem):
    contexto={}
    pro_pre_asig = PregradoSemestreAsignatura.objects.filter(programapregado=pk_pro,semestre=pk_sem)
    print(pro_pre_asig )
    franjas = FranjaHoraria.objects.filter()
    dias = Dia.objects.filter()
    existe = {}
    programacion = {}
    asignaturas = {}
    grupos = {}
    salones = {}
    sesiones = {}
    max_gru = [0]
    for asigna in pro_pre_asig:
        for dia in dias:
            for franja in franjas:
                grupo_asignatura =AsignaturaPregrado.objects.get(pk=asigna.asignaturapregrado)
                max_gru.append(grupo_asignatura.numerogrupos)
                for grupo in range(1,grupo_asignatura.numerogrupos+1):
                    if(Resultados.objects.filter(dia=dia,franjahoraria=franja,asignaturapregrado=asigna.asignaturapregrado,grupo=grupo).exists()):
                        print(Resultados.objects.filter(dia=dia,franjahoraria=franja,asignaturapregrado=asigna.asignaturapregrado))
                        salon_dia_franja = Resultados.objects.get(dia=dia,franjahoraria=franja,asignaturapregrado=asigna.asignaturapregrado,grupo=grupo)
                        asignaturas[str(salon_dia_franja.grupo)+"-"+str(dia.abreviatura)+"-"+str(franja.abreviatura)]=salon_dia_franja.asignaturapregrado
                        grupos[str(salon_dia_franja.grupo)+"-"+str(dia.abreviatura)+"-"+str(franja.abreviatura)]=salon_dia_franja.grupo
                        sesiones[str(salon_dia_franja.grupo)+"-"+str(dia.abreviatura)+"-"+str(franja.abreviatura)]=salon_dia_franja.sesiones
                        salones[str(salon_dia_franja.grupo)+"-"+str(dia.abreviatura)+"-"+str(franja.abreviatura)]=salon_dia_franja.salon
                        existe[str(salon_dia_franja.grupo)+"-"+str(dia.abreviatura)+"-"+str(franja.abreviatura)]=1
                
    num_grupos = max(max_gru)
    #print('milton----------------------------',max(max_gru))
    contexto['franjas'] = franjas
    contexto['dias'] = dias
    contexto['asignaturas'] = asignaturas
    contexto['grupos'] =  grupos
    contexto['sesiones'] = sesiones
    contexto['existe'] = existe
    contexto['salones'] = salones
    contexto['num_grupos'] = num_grupos
    contexto['programa'] = pk_pro
    contexto['semestre'] = pk_sem
    #contexto['cont'] = cont
    print('atras bueno',existe)
    
    return render(request, 'salones/sheduling_pre_sem.html', contexto)
