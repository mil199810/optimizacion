from django.contrib import admin
from import_export import resources
from import_export.admin import ImportExportModelAdmin
from .models import Instancia, Dia, CategoriaPregrado, CategoriaPosgrado, AsignaturaPregrado, Salon, FranjaHoraria, ProgramaPregrado, ProgramaPosgrado, Profesor, ProfesorDirectivo, AsignaturaProfesor, FranjaPregrado, FranjaPosgrado, SalonPregrado, DiaPregrado, SalonPosgrado, DiaPosgrado, W, PregradoSemestreAsignatura, AsignaturaGrupoMovilidad, PosgradoSemestreSesion, Resultados


class DiasAdmin(ImportExportModelAdmin):
    pass


class CategoriaPregradoAdmin(ImportExportModelAdmin):
    pass


class CategoriaPosgradoAdmin(ImportExportModelAdmin):
    pass


class AsignaturaPregradoAdmin(ImportExportModelAdmin):
    pass


class SalonAdmin(ImportExportModelAdmin):
    pass


class FranjaHorariaAdmin(ImportExportModelAdmin):
    pass


class ProgramaPregradoAdmin(ImportExportModelAdmin):
    pass


class ProgramaPosgradoAdmin(ImportExportModelAdmin):
    pass


class ProfesorAdmin(ImportExportModelAdmin):
    pass


class ProfesorDirectivoAdmin(ImportExportModelAdmin):
    pass


class AsignaturaProfesorAdmin(ImportExportModelAdmin):
    pass


class FranjaPregradoAdmin(ImportExportModelAdmin):
    pass


class FranjaPosgradoAdmin(ImportExportModelAdmin):
    pass


class SalonPregradoAdmin(ImportExportModelAdmin):
    pass


class DiaPregradoAdmin(ImportExportModelAdmin):
    pass


class SalonPosgradoAdmin(ImportExportModelAdmin):
    pass


class DiaPosgradoAdmin(ImportExportModelAdmin):
    pass



class WAdmin(ImportExportModelAdmin):
    pass


class PregradoSemestreAsignaturaAdmin(ImportExportModelAdmin):
    pass


class AsignaturaGrupoMovilidadAdmin(ImportExportModelAdmin):
    pass


class PosgradoSemestreSesionAdmin(ImportExportModelAdmin):
    pass


class ResultadosSesionAdmin(ImportExportModelAdmin):
    pass


admin.site.register(Instancia)
admin.site.register(Dia, DiasAdmin)
admin.site.register(CategoriaPregrado, CategoriaPregradoAdmin)
admin.site.register(CategoriaPosgrado, CategoriaPosgradoAdmin)
admin.site.register(AsignaturaPregrado, AsignaturaPregradoAdmin)
admin.site.register(Salon, SalonAdmin)
admin.site.register(FranjaHoraria, FranjaHorariaAdmin)
admin.site.register(ProgramaPregrado, ProgramaPregradoAdmin)
admin.site.register(ProgramaPosgrado, ProgramaPosgradoAdmin)
admin.site.register(Profesor, ProfesorAdmin)
admin.site.register(ProfesorDirectivo, ProfesorDirectivoAdmin)
admin.site.register(AsignaturaProfesor, AsignaturaProfesorAdmin)
admin.site.register(FranjaPregrado, FranjaPregradoAdmin)
admin.site.register(FranjaPosgrado, FranjaPosgradoAdmin)
admin.site.register(SalonPregrado, SalonPregradoAdmin)
admin.site.register(DiaPregrado, DiaPregradoAdmin)
admin.site.register(SalonPosgrado, SalonPosgradoAdmin)
admin.site.register(DiaPosgrado, DiaPosgradoAdmin)
admin.site.register(W, WAdmin)
admin.site.register(PregradoSemestreAsignatura, PregradoSemestreAsignaturaAdmin)
admin.site.register(AsignaturaGrupoMovilidad, AsignaturaGrupoMovilidadAdmin)
admin.site.register(PosgradoSemestreSesion, PosgradoSemestreSesionAdmin)
admin.site.register(Resultados, ResultadosSesionAdmin)

